<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Officer extends Model
{
    protected $table = 'officers';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

	    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
   	protected $fillable = [
   	    'name','degination', 'mobile', 'status', 'email', 'address', 'content', 'images', 'area_id'
   	];

    public function area()
    {
        return $this->belongsTo(Area::class);
    }

    public function agent()
    {
        return $this->hasMany(Agent::class);
    }
}
