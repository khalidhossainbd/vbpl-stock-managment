@extends('layouts.main')
@section('content')
<div class="content-wrapper">
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-12 col-sm-10">
          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">Sales History</h3>
            </div>
            <div class="card-body">
              
              
              {{-- @if ($message = Session::get('success'))
              <hr>
              <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{{ $message }}</strong>
              </div>
              <br>
              <hr>
              @endif --}}
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>Sl.</th>
                    <th>Invoice</th>
                    <th>Book Name</th>
                    <th>Agent name</th>
                    <th>Quentity</th>
                    <th>Price</th>
                    <th>Date</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($data as $item)
                    <tr>
                      <td>{{ $loop->iteration }}</td>
                      <td>{{ $item->invoice }}</td>
                      <td>{{ $item->book->name }}</td>
                      <td>{{ $item->agent->name }}</td>
                      <td>{{ $item->unit }}</td>
                      <td>{{ $item->amount }}</td>
                      <td>{{ $item->date }}</td>
                      {{-- <td>
                        <a class="btn btn-sm btn-primary" href="{{ route('books.show', $item->id) }}">Show</a>
                        <a class="btn btn-sm btn-danger" href="{{ route('books.edit', $item->id) }}">Edit</a>
                      </td> --}}
                    </tr>
                  @endforeach
                </tbody>
               
                
              </table>
            </div>
          </div>
        </div>
      </div>
      
    </div>
  </div>
  
</div>
@endsection
@section('java_script')
<script>
$(function () {
$("#example1").DataTable();
$('#example2').DataTable({
"paging": true,
"lengthChange": false,
"searching": false,
"ordering": true,
"info": true,
"autoWidth": false,
});
});
</script>
@endsection