<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sale extends Model
{
    
    protected $table = 'sales';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

	    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
   	protected $fillable = [
   	    'invoice','unit', 'date', 'book_id', 'amount', 'officer_id','area_id', 'agent_id'
   	];

    public function area()
    {
        return $this->belongsTo(Area::class);
    }
    public function officer()
    {
        return $this->belongsTo(Officer::class);
    }
    public function agent()
    {
        return $this->belongsTo(Agent::class);
    }
    public function book()
    {
        return $this->belongsTo(Book::class);
    }
}
