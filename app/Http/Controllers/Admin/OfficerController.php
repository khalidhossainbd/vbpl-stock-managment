<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Session;
use Redirect,Response;
use Auth;

use App\Area;
use App\Officer;

class OfficerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Officer::all();
        return view('admin.officers.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $areas = Area::where('status', '=', 'Active')->get();
        return view('admin.officers.create', compact('areas'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
                'name' => ['required', 'string', 'max:255'],
                'mobile'=>['required','string'],
                'email'=>['required','email'],
                'area_id'=>['required']
                ]);

        $officers = new Officer();

        $officers->create($request->all());

        return Redirect::route('officers.index')->withSuccess('Great! New Officer Add successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {  
        $data = Officer::findOrFail($id);
        return view('admin.officers.show', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Officer::findOrFail($id);
         $areas = Area::where('status', '=', 'Active')->get();
        return view('admin.officers.edit', compact('data', 'areas'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
                'name' => ['required', 'string', 'max:255'],
                'mobile'=>['required','string'],
                'email'=>['required','email'],
                ]);

        $officers = Officer::findOrFail($id);

        $officers->update($request->all());

        return Redirect::route('officers.index')->withSuccess('Great! Officer info update successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
