<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $table = 'books';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

	    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
   	protected $fillable = [
   	    'name', 'subject', 'class', 'units','purches', 'sale_rate', 'status',  'content', 'type_id'
   	];

    public function type()
    {
        return $this->belongsTo(Type::class);
    }
}
