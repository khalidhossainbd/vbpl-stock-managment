<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Session;
use Redirect,Response;
use Auth;

use App\Area;
use App\Officer;
use App\Agent;

class DistributorController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Agent::all();
        return view('admin.agents.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $areas = Area::where('status', '=', 'Active')->get();
        $officers = Officer::where('status', '=', 'Active')->get();
        return view('admin.agents.create', compact('areas', 'officers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
                'name' => ['required', 'string', 'max:255'],
                'officer_id'=>['required'],
                'area_id'=>['required','string'],
                'mobile'=>['required','string'],
                'status'=>['required','string'],
                ]);

        $agents = new Agent();

        $agents->create($request->all());

        return Redirect::route('agents.index')->withSuccess('Great! New Agent Add successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Agent::findOrFail($id);
        return view('admin.agents.show', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $areas = Area::where('status', '=', 'Active')->get();
        $officers = Officer::where('status', '=', 'Active')->get();
        $data = Agent::findOrFail($id);
        return view('admin.agents.edit', compact('areas', 'officers', 'data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
                'name' => ['required', 'string', 'max:255'],
                'officer_id'=>['required'],
                'area_id'=>['required','string'],
                'mobile'=>['required','string'],
                'status'=>['required','string'],
                ]);

        $agents = Agent::findOrFail($id);

        $agents->update($request->all());

        return Redirect::route('agents.index')->withSuccess('Great! Agent update successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
