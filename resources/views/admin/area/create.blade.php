
@extends('layouts.main')

@section('content')


  <div class="content-wrapper">

    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-12 col-sm-6">
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Territory Area Setup</h3>
              </div>

              <div class="row">
                <div class="col-12">
                  <a class="btn btn-sm btn-success m-3" href="{{ route('book_types.index') }}">Back</a>
                </div>
              </div>

              <form role="form" method="post" action="{{ route('areas.store') }}" enctype="multipart/form-data">
                @csrf
                <div class="card-body">
                  <div class="form-group">
                      <label for="">Division Name</label>
                      <select class="form-control @error('division') is-invalid @enderror" name="division" required="required">
                        <option value="" active>Select a option</option>
                        <option>Barishal</option>
                        <option>Chittagong</option>
                        <option>Dhaka</option>
                        <option>Mymensingh</option>
                        <option>Khulna</option>
                        <option>Rajshahi</option>
                        <option>Rangpur</option>
                        <option>Sylhet</option>
                      </select>
                      @error('division')
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                          </span>
                      @enderror
                  </div>
                  <div class="form-group">
                    <label for="district">District Name</label>
                    <input type="text" name="district" class="form-control" id="district" placeholder="District Name" required>
                  </div>
                  <div class="form-group">
                    <label for="title">Territory Area Name</label>
                    <input type="text" name="name" class="form-control" id="title" placeholder="Area Name" required>
                  </div>
                  
                  <div class="form-group">
                      <label for="">Status</label>
                      <select class="form-control @error('status') is-invalid @enderror" name="status" required="required">
                        <option value="" active>Select a option</option>
                        <option>Active</option>
                        <option>Inactive</option>
                      </select>
                      @error('status')
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                          </span>
                      @enderror
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
          </div>
        </div>

        
      </div>
    </div>
 
  </div>

@endsection