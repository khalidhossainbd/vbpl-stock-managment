@extends('layouts.main')
@section('content')
<div class="content-wrapper">
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-12 col-sm-10">
          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">Books List</h3>
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-12">
                  <a class="btn btn-sm btn-success" href="{{ route('books.create') }}">Add New</a>
                </div>
              </div>
              
              @if ($message = Session::get('success'))
              <hr>
              <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{{ $message }}</strong>
              </div>
              <br>
              <hr>
              @endif
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>Sl.</th>
                    <th>Type Title</th>
                    <th>Book Name</th>
                    <th>Subject</th>
                    <th>Preasent Stock</th>
                    <th>Status</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($data as $item)
                    <tr>
                      <td>{{ $loop->iteration }}</td>
                      <td>{{ $item->type->title }}</td>
                      <td>{{ $item->name }}</td>
                      <td>{{ $item->subject }}</td>
                      <td>{{ $item->units }}</td>
                      <td>{{ $item->status }}</td>
                      <td>
                        <a class="btn btn-sm btn-primary" href="{{ route('books.show', $item->id) }}">Show</a>
                        <a class="btn btn-sm btn-danger" href="{{ route('books.edit', $item->id) }}">Edit</a>
                      </td>
                    </tr>
                  @endforeach
                </tbody>
               
                
              </table>
            </div>
          </div>
        </div>
      </div>
      
    </div>
  </div>
  
</div>
@endsection
@section('java_script')
<script>
$(function () {
$("#example1").DataTable();
$('#example2').DataTable({
"paging": true,
"lengthChange": false,
"searching": false,
"ordering": true,
"info": true,
"autoWidth": false,
});
});
</script>
@endsection