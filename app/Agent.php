<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Agent extends Model
{
    protected $table = 'agents';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

	    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
   	protected $fillable = [
   	    'name', 'mobile', 'status', 'type', 'email', 'address', 'content', 'officer_id','area_id',
   	];

    public function area()
    {
        return $this->belongsTo(Area::class);
    }
    public function officer()
    {
        return $this->belongsTo(Officer::class);
    }
}
