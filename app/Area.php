<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Area extends Model
{
     protected $table = 'areas';

     /**
     * The database primary key value.
     *
     * @var string
     */
     protected $primaryKey = 'id';

 	    /**
      * Attributes that should be mass-assignable.
      *
      * @var array
      */
    	protected $fillable = [
    	    'name', 'district', 'division', 'content', 'status'
    	];

    public function officer()
    {
        return $this->hasMany(Officer::class);
    }
    
    public function agent()
    {
        return $this->hasMany(Agent::class);
    }
}
