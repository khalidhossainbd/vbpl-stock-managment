<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Session;
use Redirect,Response;
use Auth;

use App\Shistory;
use App\Book;

class ShistoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
    	$data = Shistory::all();
    	return view('admin.stocks.index', compact('data'));
    }

    public function create()
    {
    	$books = Book::where('status', '=', 'Active')->get();
    	return view('admin.stocks.create', compact('books'));
    }

    public function store(Request $request)
    {
    	// dd($request);
    	$request->validate([
                'book_id' => ['required'],
                'units'=>['required'],
                'date'=>['required'],
                ]);

        $history = new Shistory();
        $history->ShistoryID = time().rand(11111,99999);
      	$history->book_id = $request->book_id;
      	$history->units = $request->units;
      	$history->date = $request->date;
        $success = $history->save();

        if($success){
        	$book = Book::findOrFail($request->book_id);
        	$new_stock = $book->units + $request->units;

        	$book->units = $new_stock;
        	$book->update();
        }

        return Redirect::to(url('/books_stock'))->withSuccess('Great! Book stock update successfully.');
    }


    public function edit($id)
    {
    	$data = Shistory::findOrFail($id);
    	$books = Book::where('status', '=', 'Active')->get();
    	return view('admin.stocks.edit', compact('books', 'data'));
    }

    public function update()
    {
    		$request->validate([
    	            'book_id' => ['required'],
    	            'units'=>['required'],
    	            'date'=>['required'],
    	            ]);

    	    $history = new Shistory();
    	    $history->ShistoryID = time().rand(11111,99999);
    	  	$history->book_id = $request->book_id;
    	  	$history->units = $request->units;
    	  	$history->date = $request->date;
    	    $success = $history->save();

    	    if($success){
    	    	$book = Book::findOrFail($request->book_id);
    	    	$new_stock = $book->units + $request->units;

    	    	$book->units = $new_stock;
    	    	$book->update();
    	    }

    	    return Redirect::to(url('/books_stock'))->withSuccess('Great! Book stock update successfully.');
    }
}
