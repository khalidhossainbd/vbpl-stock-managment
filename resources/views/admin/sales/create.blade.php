@extends('layouts.main')
@section('content')
<div class="content-wrapper">
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-12">
          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">Sales</h3>
            </div>
            @if ($message = Session::get('success'))
            <div class="row p-3">
              <div class="col-12">
                <div class="alert alert-success alert-block">
                  <button type="button" class="close" data-dismiss="alert">×</button>
                  <strong>{{ $message }}</strong>
                </div>
                <br>
              </div>
            </div>
            @endif
            {{-- <div class="row p-3">
              <div class="col-12">
                <a class="btn btn-sm btn-success" href="{{ route('officers.index') }}">Back</a>
              </div>
            </div> --}}
            <form role="form" method="post" action="{{ route('sales.store') }}" enctype="multipart/form-data">
              @csrf
              <div class="card-body">
                <div class="row">
                  <div class="col-12 col-sm-6">
                    <div class="form-group">
                      <label for="">Territory Area</label>
                      <select class="form-control @error('area_id') is-invalid @enderror" name="area_id" required="required">
                        <option value="" active>Select a option</option>
                        @foreach($area as $areas)
                        <option value="{{ $areas->id }}">{{ $areas->name }}</option>
                        @endforeach
                      </select>
                      @error('area_id')
                      <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                      </span>
                      @enderror
                    </div>
                    <div class="form-group">
                      <label for="">Territory Sales Officer</label>
                      <select class="form-control @error('officer_id') is-invalid @enderror" name="officer_id" required="required">
                        <option value="" active>Select a option</option>
                        @foreach($officer as $officers)
                        <option value="{{ $officers->id }}">{{ $officers->name }}</option>
                        @endforeach
                      </select>
                      @error('officer_id')
                      <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                      </span>
                      @enderror
                    </div>
                  </div>
                  <div class="col-12 col-sm-6">
                    
                    <div class="form-group">
                      <label for="">Territory Agent</label>
                      <select class="form-control @error('agent_id') is-invalid @enderror" name="agent_id" required="required">
                        <option value="" active>Select a option</option>
                        @foreach($agent as $agents)
                        <option value="{{ $agents->id }}">{{ $agents->name }}</option>
                        @endforeach
                      </select>
                      @error('agent_id')
                      <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                      </span>
                      @enderror
                    </div>
                    <div class="form-group">
                      <label for="date">Sale Date</label>
                      <input type="date" name="date" class="form-control" id="date" placeholder="Sale Date" required  >
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-12">
                    <table class="table table-bordered" id="dynamic_field">
                      <thead>
                        <th>Select Book</th>
                        <th>Book Price</th>
                        <th>Book Quentity</th>
                        <th>Total Price</th>
                        <th>Action</th>
                      </thead>
                      <tr>
                        <td>
                          <div class="form-group">
                            <select class="form-control @error('book_id') is-invalid @enderror" name="book_id[]" required="required">
                              <option value="" active>Select a option</option>
                              @foreach($books as $book)
                              <option value="{{ $book->id }}">{{ $book->name }} ( {{ $book->class }} ) </option>
                              @endforeach
                            </select>
                          </div>
                        </td>
                        <td>
                          <div class="form-group">
                            <input type="text" name="unitprice[]" class="form-control"  placeholder="Book Price" required>
                          </div>
                        </td>
                        <td>
                          <div class="form-group">
                            <input type="text" name="unit[]" class="form-control"  placeholder="Book Quentity" required>
                          </div>
                        </td>
                        <td>
                          <div class="form-group">
                            <input type="text" name="price[]" class="form-control" id="email" placeholder="Price" required>
                          </div>
                        </td>
                        <td>
                          <button type="button" name="add" id="add" class="btn btn-success">
                            <i class="fas fa-plus"></i>
                          </button>
                        </td>
                      </tr>
                    </table>
                    
                  </div>
                </div>
              </div>
              <!-- /.card-body -->
              <div class="card-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
        </div>
      </div>
      
    </div>
  </div>
  
</div>
@endsection
@section('java_script')
<script>
$(document).ready(function(){
var i=1;
$('#add').click(function(){
i++;
$('#dynamic_field').append('<tr id="row'+i+'"><td>'+
  '<div class="form-group">'+
    '<select class="form-control" name="book_id[]">'+
      '<option value="" active>Select a option</option>'+
      '@foreach($books as $book)'+
      '<option value="{{ $book->id }}">{{ $book->name }}  ( {{ $book->class }} )</option>'+
      '@endforeach'+
    '</select>'+
  '</div>'+
'</td>'+
'<td><input type="text" name="unitprice[]" class="form-control"  placeholder="Book Price" required></td><td><input type="text" name="unit[]" placeholder="Book quentity" class="form-control" /></td><td><input type="text" name="price[]" placeholder="Price" class="form-control" /></td><td><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove">X</button></td></tr>');
});
$(document).on('click', '.btn_remove', function(){
var button_id = $(this).attr("id");
$('#row'+button_id+'').remove();
});
$('#submit').click(function(){
$.ajax({
url:"name.php",
method:"POST",
data:$('#add_name').serialize(),
success:function(data)
{
alert(data);
$('#add_name')[0].reset();
}
});
});
});
</script>
@endsection