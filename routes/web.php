<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/books_stock', 'Admin\BookController@stock');

Route::get('/saler-wise-sale-report', 'Admin\ReportController@salerReport');
Route::get('/agent-wise-sale-report', 'Admin\ReportController@agentReport');


Route::resource('/book_types', 'Admin\\TypeController');
Route::resource('/books', 'Admin\\BookController');
Route::resource('/book_stock', 'Admin\\ShistoryController');

Route::resource('/areas', 'Admin\\AreaController');
Route::resource('/officers', 'Admin\\OfficerController');
Route::resource('/agents', 'Admin\\DistributorController');

Route::resource('/sales', 'Admin\\SaleController');

Route::get('/{any}', function ($any) {

  return Redirect::to('/');

})->where('any', '.*');
