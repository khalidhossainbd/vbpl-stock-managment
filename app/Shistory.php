<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shistory extends Model
{
    protected $table = 'shistories';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

	    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
   	protected $fillable = [
   	    'ShistoryID', 'book_id', 'date', 'units', 'status'
   	];

    public function book()
    {
        return $this->belongsTo(Book::class);
    }
}
