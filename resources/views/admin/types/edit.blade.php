@extends('layouts.main')
@section('content')
<div class="content-wrapper">
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-12 col-sm-6">
          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">Book Type Edit</h3>
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-12">
                  <a class="btn btn-sm btn-success" href="{{ route('book_types.index') }}">Back</a>
                </div>
              </div>
              <form role="form" method="post" action="{{ route('book_types.update', $data->id) }}" enctype="multipart/form-data">
                @csrf
                @method('Patch')  
                <div class="card-body">
                  <div class="form-group">
                    <label for="title">Type Title</label>
                    <input type="text" name="title" class="form-control" id="title" value="{{ $data->title }}">
                  </div>
                  
                  <div class="form-group">
                    <label for="">Status</label>
                    <select class="form-control @error('status') is-invalid @enderror" name="status" required="required">
                      <option value="" active>Select a option</option>
                      <option {{ $data->status == 'Active' ? 'selected' : '' }}>Active</option>
                      <option {{ $data->status == 'Inactive' ? 'selected' : '' }}>Inactive</option>
                    </select>
                    @error('status')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="content">Content</label>
                    <textarea  class="form-control" rows="5" name="content">{{ $data->content }}</textarea>
                  </div>
                  
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Update</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      
    </div>
  </div>
  
</div>
@endsection