
@extends('layouts.main')

@section('content')


  <div class="content-wrapper">

    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-12 col-sm-6">
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Book Type Setup</h3>
              </div>

              <div class="row">
                <div class="col-12">
                  <a class="btn btn-sm btn-success p-3" href="{{ route('book_types.index') }}">Back</a>
                </div>
              </div>

              <form role="form" method="post" action="{{ route('book_types.store') }}" enctype="multipart/form-data">
                @csrf
                <div class="card-body">
                  <div class="form-group">
                    <label for="title">Type Title</label>
                    <input type="text" name="title" class="form-control" id="title" placeholder="Title">
                  </div>
                  
                  <div class="form-group">
                      <label for="">Status</label>
                      <select class="form-control @error('status') is-invalid @enderror" name="status" required="required">
                        <option value="" active>Select a option</option>
                        <option>Active</option>
                        <option>Inactive</option>
                      </select>
                      @error('status')
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                          </span>
                      @enderror
                  </div>
                  <div class="form-group">
                    <label for="content">Content</label>
                    <textarea  class="form-control" rows="5" name="content"></textarea>
                </div>
                  
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
          </div>
        </div>

        
      </div>
    </div>
 
  </div>

@endsection