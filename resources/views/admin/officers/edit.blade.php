
@extends('layouts.main')

@section('content')


  <div class="content-wrapper">

    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-12">
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Sale Office Info Edit</h3>
              </div>

              <div class="row p-3">
                <div class="col-12">
                  <a class="btn btn-sm btn-success" href="{{ route('officers.index') }}">Back</a>
                </div>
              </div>

              <form role="form" method="post" action="{{ route('officers.update', $data->id) }}" enctype="multipart/form-data">
                @csrf
                @method('Patch')
                <div class="card-body">
                  <div class="row">
                    <div class="col-12 col-sm-6">
                      <div class="form-group">
                        <label for="name">Officer Name</label>
                        <input type="text" name="name" class="form-control" id="name" placeholder="Title" required value="{{ $data->name }}" >
                      </div>
                      <div class="form-group">
                        <label for="degination">Offical Degination</label>
                        <input type="text" name="degination" class="form-control" id="degination" placeholder="Degination" required value="{{ $data->degination}}" >
                      </div>
                      <div class="form-group">
                        <label for="mobile">Mobile Number</label>
                        <input type="text" name="mobile" class="form-control" id="mobile" placeholder="Mobile Number" required value="{{ $data->mobile }}" >
                      </div>
                      <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" name="email" class="form-control" id="email" placeholder="Email Address" required value="{{ $data->email }}">
                      </div>
                      
                    </div>
                    <div class="col-12 col-sm-6">
                      <div class="form-group">
                          <label for="">Territory Area</label>
                          <select class="form-control @error('area_id') is-invalid @enderror" name="area_id" required="required">
                            <option value="" active>Select a option</option>
                            @foreach($areas as $area)
                              <option value="{{ $area->id }}" {{ $data->area_id  == $area->id ? 'selected' : ''}}>{{ $area->name }}</option>
                            @endforeach
                          </select>
                          @error('area_id')
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $message }}</strong>
                              </span>
                          @enderror
                      </div>
                      <div class="form-group">
                          <label for="">Status</label>
                          <select class="form-control @error('status') is-invalid @enderror" name="status" required="required">
                            <option value="" active>Select a option</option>
                            <option {{ $data->status == 'Active' ? 'selected' : '' }}>Active</option>
                            <option {{ $data->status == 'Inactive' ? 'selected' : '' }}>Inactive</option>
                          </select>
                          @error('status')
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $message }}</strong>
                              </span>
                          @enderror
                      </div>
                      <div class="form-group">
                        <label for="content">Content</label>
                        <textarea  class="form-control" rows="3" name="content">{{ $data->content }}</textarea>
                      </div>
                      
                      
                    </div>
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
          </div>
        </div>

        
      </div>
    </div>
 
  </div>

@endsection