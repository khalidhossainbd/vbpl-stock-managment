
@extends('layouts.main')

@section('content')


  <div class="content-wrapper">

    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-12">
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Territory Agent Setup</h3>
              </div>

              <div class="row p-3">
                <div class="col-12">
                  <a class="btn btn-sm btn-success" href="{{ route('agents.index') }}">Back</a>
                </div>
              </div>

              <form role="form" method="post" action="{{ route('agents.store') }}" enctype="multipart/form-data">
                @csrf
                <div class="card-body">
                  <div class="row">
                    <div class="col-12 col-sm-6">
                      <div class="form-group">
                          <label for="">Territory Area</label>
                          <select class="form-control @error('area_id') is-invalid @enderror" name="area_id" required="required">
                            <option value="" active>Select a option</option>
                            @foreach($areas as $area)
                              <option value="{{ $area->id }}">{{ $area->name }}</option>
                            @endforeach
                          </select>
                          @error('area_id')
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $message }}</strong>
                              </span>
                          @enderror
                      </div>
                      <div class="form-group">
                          <label for="">Territory Officer</label>
                          <select class="form-control @error('officer_id') is-invalid @enderror" name="officer_id" required="required">
                            <option value="" active>Select a option</option>
                            @foreach($officers as $officer)
                              <option value="{{ $officer->id }}">{{ $officer->name }}</option>
                            @endforeach
                          </select>
                          @error('officer_id')
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $message }}</strong>
                              </span>
                          @enderror
                      </div>
                      <div class="form-group">
                        <label for="name">Agent Name</label>
                        <input type="text" name="name" class="form-control" id="name" placeholder="Agent Name" required value="{{ old('name') }}" >
                      </div>
                      <div class="form-group">
                        <label for="mobile">Mobile Number</label>
                        <input type="text" name="mobile" class="form-control" id="mobile" placeholder="Mobile Number" required value="{{ old('mobile') }}" >
                      </div>
                      <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" name="email" class="form-control" id="email" placeholder="Email Address" required value="{{ old('email') }}">
                      </div>
                      
                    </div>
                    <div class="col-12 col-sm-6">
                      <div class="form-group">
                          <label for="">Type of Territory Agent</label>
                          <select class="form-control @error('type') is-invalid @enderror" name="type" required="required">
                            <option value="" active>Select a option</option>
                            <option>Institute</option>
                            <option>Distributor</option>
                          </select>
                          @error('type')
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $message }}</strong>
                              </span>
                          @enderror
                      </div>
                      <div class="form-group">
                          <label for="">Status</label>
                          <select class="form-control @error('status') is-invalid @enderror" name="status" required="required">
                            <option value="" active>Select a option</option>
                            <option>Active</option>
                            <option>Inactive</option>
                          </select>
                          @error('status')
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $message }}</strong>
                              </span>
                          @enderror
                      </div>
                      <div class="form-group">
                        <label for="address">Address</label>
                        <textarea  class="form-control" rows="3" name="address"></textarea>
                      </div>
                      <div class="form-group">
                        <label for="content">Content</label>
                        <textarea  class="form-control" rows="3" name="content"></textarea>
                      </div>
                      
                    </div>
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
          </div>
        </div>

        
      </div>
    </div>
 
  </div>

@endsection