<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Touheed Publication | Dashboard</title>
    <link rel="icon" href="{{ asset('images/publication.png') }}" type="image/gif" sizes="16x16">
    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="{{ asset('back/plugins/fontawesome-free/css/all.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('back/dist/css/adminlte.min.css') }}">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  </head>
  <body class="hold-transition sidebar-mini">
    <div class="wrapper">

      @include('partials.navber')
      @include('partials.sidebar')

      @yield('content')

      <!-- Main Footer -->
      <footer class="main-footer">
        <!-- To the right -->
        {{-- <div class="float-right d-none d-sm-inline">
          Anything you want
        </div> --}}
        <!-- Default to the left -->
        <strong>Copyright &copy; 2020 <a href="#">Touheed Publication</a>.</strong> All rights reserved.
      </footer>

    </div>
    <!-- jQuery -->
    <script src="{{ asset('back/plugins/jquery/jquery.min.js') }}"></script>
    <!-- Bootstrap 4 -->
    <script src="{{ asset('back/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <!-- AdminLTE App -->
    <script src="{{ asset('back/dist/js/adminlte.min.js') }}"></script>
    <script src="{{ asset('back/plugins/datatables/jquery.dataTables.js')}}"></script>
    <script src="{{ asset('back/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>

    @yield('java_script')
  </body>
</html>