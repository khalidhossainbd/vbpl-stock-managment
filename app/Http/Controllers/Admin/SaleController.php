<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Session;
use Redirect,Response;
use Auth;

use App\Area;
use App\Officer;
use App\Agent;
use App\Book;
use App\Sale;

class SaleController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Sale::all();
        return view('admin.sales.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $area = Area::where('status', '=', 'Active')->get();
        $officer = Officer::where('status', '=', 'Active')->get();
        $agent = Agent::where('status', '=', 'Active')->get();
        $books = Book::where('status', '=', 'Active')->get();
        return view('admin.sales.create', compact('area', 'officer', 'agent', 'books'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {        

        $request->validate([
                'date' => ['required'],
                'officer_id'=>['required'],
                'area_id'=>['required'],
                ]);

        $num = count($request->book_id);
        $invoice = rand(1000, 9999).time();
        for($i = 0; $i<$num; $i++){
            $sales = new Sale();
            $sales->invoice = $invoice;
            $sales->date = $request->date;
            $sales->officer_id = $request->officer_id;
            $sales->area_id = $request->area_id;
            $sales->agent_id = $request->agent_id;
            $sales->book_id = $request->book_id[$i];
            $sales->unit = $request->unit[$i];
            $sales->amount = $request->price[$i];

            $success = $sales->save($request->all());

            if($success){
                $book = Book::findOrFail($request->book_id[$i]);
                $new_stock = $book->units - $request->unit[$i];

                $book->units = $new_stock;
                $book->update();
            }
        }

        return back()->withSuccess('Great! Sale Add successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
