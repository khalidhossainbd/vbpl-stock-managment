<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
  <!-- Brand Logo -->
  <a href="#" class="brand-link">
    {{-- <img src="" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
    style="opacity: .8"> --}}
    <p class="text-center font-weight-bold m-0">Touheed Publiction</p>
  </a>
  <!-- Sidebar -->
  <div class="sidebar">
    <!-- Sidebar user panel (optional) -->
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
      {{-- <div class="image">
        <img src="dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
      </div> --}}
      <div class="info">
        <a href="{{ url('/home') }}" class="d-block">Dashboard</a>
      </div>
    </div>
    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <li class="nav-item has-treeview">
          <a href="#" class="nav-link">
            <i class="nav-icon fas fa-tachometer-alt"></i>
            <p>
              Salers Setup
              <i class="right fas fa-angle-left"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="{{ route('areas.index') }}" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Territory Area</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{ route('officers.index') }}" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Territory sales officer</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{ route('agents.index') }}" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Agents</p>
              </a>
            </li>
          </ul>
        </li>
        
        <li class="nav-item has-treeview">
          <a href="#" class="nav-link">
            <i class="nav-icon fas fa-tree"></i>
            <p>
              Books Setup
              <i class="fas fa-angle-left right"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="{{ route('book_types.index') }}" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Book Type</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{ route('books.index') }}" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Books List</p>
              </a>
            </li>
            {{-- <li class="nav-item">
              <a href="pages/UI/buttons.html" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Buttons</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="pages/UI/sliders.html" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Sliders</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="pages/UI/modals.html" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Modals & Alerts</p>
              </a>
            </li> --}}
            
          </ul>
        </li>
        <li class="nav-item has-treeview">
          <a href="{{ route('sales.create') }}" class="nav-link active">
            <i class="fas fa-balance-scale"></i>
            <p>
              Sales
            </p>
          </a>
        </li>
        <li class="nav-item">
          <a href="{{ url('/books_stock') }}" class="nav-link">
            <i class="nav-icon fas fa-th"></i>
            <p>
              Stock List
            </p>
          </a>
        </li>
        <li class="nav-item">
          <a href="{{ route('sales.index') }}" class="nav-link">
            <i class="nav-icon fas fa-th"></i>
            <p>
              Sales History
            </p>
          </a>
        </li>
        <li class="nav-item">
          <a href="{{ route('book_stock.index') }}" class="nav-link">
            <i class="nav-icon fas fa-th"></i>
            <p>
              Stock History
            </p>
          </a>
        </li>
        <li class="nav-item has-treeview">
          <a href="#" class="nav-link">
            <i class="nav-icon fas fa-tree"></i>
            <p>
              Reports
              <i class="fas fa-angle-left right"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="{{ url('/saler-wise-sale-report') }}" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Saler wise sales report</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{ url('/agent-wise-sale-report') }}" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Agent wise sales report</p>
              </a>
            </li>
            {{-- <li class="nav-item">
              <a href="#" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Buttons</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="#" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Sliders</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="#" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Modals & Alerts</p>
              </a>
            </li> --}}
            
          </ul>
        </li>
      </ul>
    </nav>
    <!-- /.sidebar-menu -->
  </div>
  <!-- /.sidebar -->
</aside>