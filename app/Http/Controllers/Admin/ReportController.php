<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Session;
use Redirect,Response;
use Auth;

use App\Area;
use App\Officer;
use App\Agent;
use App\Book;
use App\Sale;


class ReportController extends Controller
{

	public function __construct()
    {
        $this->middleware('auth');
    }


    public function salerReport()
    {
    	$data = Sale::orderBy('officer_id', 'desc')->get();
    	return view('admin.reports.salerReport', compact('data'));
    }

    public function agentReport()
    {
    	return view('admin.reports.agentReport');
    }
}
