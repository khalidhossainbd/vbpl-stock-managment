
@extends('layouts.main')

@section('content')


  <div class="content-wrapper">

    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-12 col-sm-6">
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Book Stock Update</h3>
              </div>

              <div class="row">
                <div class="col-12">
                  <a class="btn btn-sm btn-success m-3" href="{{ url('/books_stock') }}">Back</a>
                </div>
              </div>

              <form role="form" method="post" action="{{ route('book_stock.store') }}" enctype="multipart/form-data">
                @csrf
                <div class="card-body">
                  <div class="form-group">
                    <label for="date">Date</label>
                    <input type="date" name="date" class="form-control" id="date" placeholder="Stock Update Date">
                  </div>
                  
                  <div class="form-group">
                      <label for="">Book Name</label>
                      <select class="form-control @error('book_id') is-invalid @enderror" name="book_id" required="required">
                        <option value="" active>Select a option</option>
                        @foreach($books as $book)
                        <option value="{{ $book->id }}">{{ $book->name }}</option>
                        @endforeach

                      </select>
                      @error('book_id')
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                          </span>
                      @enderror
                  </div>
                  <div class="form-group">
                    <label for="units">Update Amount</label>
                    <input type="units" name="units" class="form-control" id="units" placeholder="Total Update Books">
                  </div>
                  
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
          </div>
        </div>

        
      </div>
    </div>
 
  </div>

@endsection