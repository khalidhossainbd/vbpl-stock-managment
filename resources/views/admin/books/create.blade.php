
@extends('layouts.main')

@section('content')


  <div class="content-wrapper">

    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-12">
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Book Setup</h3>
              </div>

              <div class="row p-3">
                <div class="col-12">
                  <a class="btn btn-sm btn-success" href="{{ route('books.index') }}">Back</a>
                </div>
              </div>

              <form role="form" method="post" action="{{ route('books.store') }}" enctype="multipart/form-data">
                @csrf
                <div class="card-body">
                  <div class="row">
                    <div class="col-12 col-sm-6">
                      <div class="form-group">
                          <label for="">Book Type</label>
                          <select class="form-control @error('type_id') is-invalid @enderror" name="type_id" required="required">
                            <option value="" active>Select a option</option>
                            @foreach($types as $type)
                            <option value="{{ $type->id }}">{{ $type->title }}</option>
                            @endforeach
                          </select>
                          @error('type_id')
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $message }}</strong>
                              </span>
                          @enderror
                      </div>
                      <div class="form-group">
                        <label for="name">Book Title</label>
                        <input type="text" name="name" class="form-control" id="name" placeholder="Title" required value="{{ old('name') }}" >
                      </div>
                      <div class="form-group">
                        <label for="subject">Book subject</label>
                        <input type="text" name="subject" class="form-control" id="subject" placeholder="Book subject" required value="{{ old('subject') }}" >
                      </div>
                      <div class="form-group">
                        <label for="class">Book for class</label>
                        <input type="text" name="class" class="form-control" id="class" placeholder="Book class" required value="{{ old('class') }}" >
                      </div>
                      
                      <div class="form-group">
                        <label for="content">Content</label>
                        <textarea  class="form-control" rows="3" name="content"></textarea>
                      </div>
                    </div>
                    <div class="col-12 col-sm-6">
                      <div class="form-group">
                        <label for="units">Present Stock</label>
                        <input type="text" name="units" class="form-control" id="units" placeholder="Present Stock" required value="{{ old('units') }}">
                      </div>
                      <div class="form-group">
                        <label for="purches">Purches rate</label>
                        <input type="text" name="purches" class="form-control" id="purches" placeholder="Purches rate" required value="{{ old('purches') }}">
                      </div>
                      <div class="form-group">
                        <label for="sale_rate">Sale rate</label>
                        <input type="text" name="sale_rate" class="form-control" id="sale_rate" placeholder="Sale rate" required value="{{ old('sale_rate') }}">
                      </div>
                      
                      <div class="form-group">
                          <label for="">Status</label>
                          <select class="form-control @error('status') is-invalid @enderror" name="status" required="required">
                            <option value="" active>Select a option</option>
                            <option>Active</option>
                            <option>Inactive</option>
                          </select>
                          @error('status')
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $message }}</strong>
                              </span>
                          @enderror
                      </div>
                      
                    </div>
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
          </div>
        </div>

        
      </div>
    </div>
 
  </div>

@endsection