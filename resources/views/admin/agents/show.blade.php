@extends('layouts.main')
@section('content')
<div class="content-wrapper">
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-12 col-sm-8">
          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">Territory Agents List</h3>
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-12">
                  <a class="btn btn-sm btn-success" href="{{ route('agents.index') }}">Back </a>
                </div>
              </div>
              
              @if ($message = Session::get('success'))
              <hr>
              <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{{ $message }}</strong>
              </div>
              <br>
              <hr>
              @endif
              <table  class="table table-bordered table-hover my-3">
                <tr>
                  <th>Territory Area</th>
                  <td>{{ $data->area->name }}</td>
                </tr>
                  <tr>
                    <th>Territory Officer Name</th>
                    <td>{{ $data->officer->name }}</td>
                  </tr>
                  <tr>
                    <th>Agent Name</th>
                    <td>{{ $data->name }}</td>
                  </tr>
                  <tr>
                    <th>Mobile Number</th>
                    <td>{{ $data->mobile }}</td>
                  </tr>
                  <tr>
                    <th>Email</th>
                    <td>{{ $data->email }}</td>
                  </tr>
                  <tr>
                    <th>Address</th>
                    <td>{{ $data->address }}</td>
                  </tr>
                  <tr>
                    <th>Content</th>
                    <td>{{ $data->content }}</td>
                  </tr>
              </table>
            </div>
          </div>
        </div>
      </div>
      
    </div>
  </div>
  
</div>
@endsection
@section('java_script')
<script>
$(function () {
$("#example1").DataTable();
$('#example2').DataTable({
"paging": true,
"lengthChange": false,
"searching": false,
"ordering": true,
"info": true,
"autoWidth": false,
});
});
</script>
@endsection