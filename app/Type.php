<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
        protected $table = 'types';

        /**
        * The database primary key value.
        *
        * @var string
        */
        protected $primaryKey = 'id';

    	    /**
         * Attributes that should be mass-assignable.
         *
         * @var array
         */
       	protected $fillable = [
       	    'title','status','content', 
       	];

    public function book()
    {
        return $this->hasMany(Book::class);
    }
}
