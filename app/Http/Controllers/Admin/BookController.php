<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Session;
use Redirect,Response;
use Auth;

use App\Type;
use App\Book;

class BookController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Book::all();
        return view('admin.books.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $types = Type::where('status', '=', 'Active')->get();
        return view('admin.books.create', compact('types'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        $request->validate([
                'name' => ['required', 'string', 'max:255'],
                'subject'=>['required','string'],
                'class'=>['required','string'],
                'type_id'=>['required','string'],
                'sale_rate'=>['required','string'],
                'status'=>['required','string'],
                ]);

        $books = new Book();

        $books->create($request->all());

        return Redirect::route('books.index')->withSuccess('Great! New Book Add successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Book::findOrFail($id);
        return view('admin.books.show', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Book::findOrFail($id);
        $types = Type::where('status', '=', 'Active')->get();
        return view('admin.books.edit', compact('types', 'data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $request->validate([
                'name' => ['required', 'string', 'max:255'],
                'subject'=>['required','string'],
                'class'=>['required','string'],
                'type_id'=>['required','string'],
                'sale_rate'=>['required','string'],
                'status'=>['required','string'],
                ]);

        $books = Book::findOrFail($id);

        $books->update($request->all());

        return Redirect::route('books.index')->withSuccess('Great! Book Update successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }



    public function stock()
    {
        $data = Book::where('status', '=', 'Active')->get();
        return view('pages.book_stock', compact('data'));
    }
}
