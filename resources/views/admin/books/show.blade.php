@extends('layouts.main')
@section('content')
<div class="content-wrapper">
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-12 col-sm-8">
          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">Book Details</h3>
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-12">
                  <a class="btn btn-sm btn-success my-3" href="{{ route('books.index') }}">Back</a>
                </div>
              </div>
              
              @if ($message = Session::get('success'))
              <hr>
              <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{{ $message }}</strong>
              </div>
              <br>
              <hr>
              @endif
              <table class="table table-bordered table-striped">
                <tr>
                  <th>Book Type</th>
                  <td>{{  $data->type->title }}</td>
                </tr>
                <tr>
                  <th>Book Name</th>
                  <td>{{  $data->name }}</td>
                </tr>
                <tr>
                  <th>Book for class</th>
                  <td>{{  $data->class }}</td>
                </tr>
                <tr>
                  <th>Book purches rate</th>
                  <td>{{  $data->purches }}</td>
                </tr>
                <tr>
                  <th>Book sale rate</th>
                  <td>{{  $data->sale_rate}}</td>
                </tr>
                <tr>
                  <th>Book Present stock</th>
                  <td>{{  $data->units}}</td>
                </tr>
                <tr>
                  <th>Status</th>
                  <td>{{  $data->status}}</td>
                </tr>
                <tr>
                  <th>Content</th>
                  <td>{{  $data->content}}</td>
                </tr>
                
              </table>
            </div>
          </div>
        </div>
      </div>
      
    </div>
  </div>
  
</div>
@endsection
